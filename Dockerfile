FROM ubuntu:18.04

RUN apt update &&\
    apt install -y \
    ocserv \
    vim \
    iptables

WORKDIR /etc/ocserv
COPY ./ocserv_files/* ./
RUN mkdir certificates && cd certificates && \
    mv ../certificate_generator.sh ./ && \
    chmod 755 certificate_generator.sh

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod 777 /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["ocserv", "-c", "/etc/ocserv/ocserv.conf", "-f"]
# CMD exec /bin/sh -c "trap : TERM INT; (while true; do sleep 1000; done) & wait"
