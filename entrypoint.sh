#!/bin/bash


set -e

sysctl -w net.ipv4.ip_forward=1

# Create ssl certificate
KEY_FILE=/etc/ocserv/certificates/rootCA.crt
CERT_FILE=/etc/ocserv/certificates/rootCA.key

if [[ ! -f "$KEY_FILE" || ! -f "$CERT_FILE" ]]
then
    /etc/ocserv/certificates/certificate_generator.sh example.com
fi

# Enable NAT forwarding
iptables -t nat -A POSTROUTING -j MASQUERADE
iptables -A FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu

# Enable TUN device
mkdir -p /dev/net
mknod /dev/net/tun c 10 200 || true
chmod 600 /dev/net/tun

# Run OpennConnect Server
#ocserv -c /etc/ocserv/ocserv.conf -f
exec "$@"

